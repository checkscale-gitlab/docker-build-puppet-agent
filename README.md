# Docker images: Puppet agent

OSs and puppet versions are available as different tags in [the repo registry](https://gitlab.com/sjugge/docker/build-images/docker-build-puppet-agent/container_registry).

```plaintext
registry.gitlab.com/sjugge/docker/build-images/docker-build-puppet-agent:<os>-<os major version>-puppet-<puppet agent major version>
```

## Local use

Build all images:

```sh
for d in $(ls | grep -v README);do docker build -t $d $d;done
```

List images and the Puppet version installed:

```sh
for d in $(ls | grep -v README);do echo "$d: " && docker run --rm -it $d --version;done
```
